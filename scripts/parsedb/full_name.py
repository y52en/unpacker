import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


names = {
    19: '一井 透',
    20: '百木 るん',
    21: '西 由宇子',
    22: '天王寺 渚',
    23: '野山 美歩',
    116: '保登 心愛',
    117: '香風 智乃',
    143: '天々座 理世',
    144: '宇治松 千夜',
    145: '桐間 紗路',
    151: '条河 麻耶',
    152: '奈津 恵',
    168: '保登 モカ',
}


def handle():
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    for named in named_list:
        named['fullName'] = names.get(
            named['m_NamedType'], named['m_FullName'])

    with open(pwd + database + 'NamedList.json', 'w') as f:
        json.dump(named_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
