import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'ADVList.json') as f:
        adv_list = json.load(f)
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)

    cross_adv_list = {}

    def add_cross_adv(name1, name2, adv_id):
        if name1 not in cross_adv_list:
            cross_adv_list[name1] = []
        cross_adv_list[name1].append({
            'm_NamedType': name2,
            'm_AdvID': adv_id,
        })

    for adv in adv_list:
        if adv['m_Category'] != 4:
            continue
        name1, name2 = adv['m_NamedType']
        add_cross_adv(name1, name2, adv['m_AdvID'])
        add_cross_adv(name2, name1, adv['m_AdvID'])

    for named in named_list:
        named['crossAdvList'] = cross_adv_list.get(named['m_NamedType'], [])

    with open(pwd + database + 'NamedList.json', 'w') as f:
        json.dump(named_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
