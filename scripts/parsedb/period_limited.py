import os
import json


pwd = (os.path.dirname(__file__) or '.') + '/../../'
database = 'assets/app/staticresources/bundlesource/database/'


def handle():
    with open(pwd + database + 'CharacterList.json') as f:
        character_list = json.load(f)
    with open(pwd + database + 'CharacterOverride.json') as f:
        character_override = json.load(f)
    with open(pwd + database + 'NamedList.json') as f:
        named_list = json.load(f)
        names = {
            named['m_NamedType']: named
            for named in named_list
        }

    character_override_ids = []
    for item in character_override:
        character_override_ids += item['m_CharaIDs']

    for character in character_list:
        character['isPeriodLimited'] = not character['isDistributed'] and (
            '【' in character['m_Name'] or
            character['m_CharaID'] in character_override_ids or
            names[character['m_NamedType']]['m_TitleType'] == 34
        )

    with open(pwd + database + 'CharacterList.json', 'w') as f:
        json.dump(character_list, f, ensure_ascii=False)


if __name__ == '__main__':
    handle()
